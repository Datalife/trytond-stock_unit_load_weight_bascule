# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Not, Len
from trytond.wizard import Button
from trytond.rpc import RPC

__all__ = ['Unitload', 'UnitLoadWeightData', 'WeighUL']


class Unitload(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.__rpc__['weigh_from_bascule'] = RPC(readonly=False,
            instantiate=None)

    @classmethod
    def _get_bascule_pattern(cls):
        return {'model': 'stock.unit_load'}

    @classmethod
    def weigh_from_bascule(cls, code):
        pool = Pool()
        Unitload = pool.get('stock.unit_load')
        User = pool.get('res.user')

        if isinstance(code, Unitload):
            unitload = code
        else:
            unitload = Unitload.search(
                Unitload._get_barcode_search_domain(code), limit=1)
            if not unitload:
                return
            unitload, = unitload

        user = User(Transaction().user)
        bascule = user.get_bascule(Unitload._get_bascule_pattern())
        if not bascule:
            return
        value = bascule.read_measure()
        unitload.gross_weight = value
        unitload.on_change_gross_weight()
        unitload.save()


class UnitLoadWeightData(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.weigh.data'

    bascule = fields.Many2One('stock.bascule', 'Bascule', readonly=True,
        states={
            'invisible': Len(Eval('context', {}).get('active_ids')) > 1
        })


class UnitLoadWeightStart(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.weigh.start'

    last_unit_load = fields.Many2One('stock.unit_load', "Last Unit Load",
        readonly=True, states={
            'invisible': ~Eval('last_unit_load')
        },
        depends=['last_unit_load'])
    last_gross_weight = fields.Float("Gross weight", readonly=True,
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'invisible': ~Eval('last_unit_load')
        },
        depends=['weight_unit_digits', 'last_unit_load'])
    last_tare = fields.Float("Tare", readonly=True,
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'invisible': ~Eval('last_unit_load')
        },
        depends=['weight_unit_digits', 'last_unit_load'])
    last_net_weight = fields.Float("Net weight", readonly=True,
        digits=(16, Eval('weight_unit_digits', 2)),
        states={
            'invisible': ~Eval('last_unit_load')
        },
        depends=['weight_unit_digits', 'last_unit_load'])
    weight_unit_digits = fields.Integer('Weight UOM digits')

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('//separator[@id="last_ul"]', 'states', {
                'invisible': ~(Eval('last_unit_load'))}),
        ]


class WeighUL(metaclass=PoolMeta):
    __name__ = 'stock.unit_load.weigh'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.data.buttons.append(
            Button('Get weight', 'data', 'tryton-launch', states={
                'invisible': Not(Eval('bascule'))
            })
        )

    def default_data(self, fields):
        pool = Pool()
        User = pool.get('res.user')
        UL = pool.get('stock.unit_load')

        values = super().default_data(fields)
        if len(self.records) == 1:
            user = User(Transaction().user)
            bascule = user.get_bascule(UL._get_bascule_pattern())
            # read from bascule if exists
            if bascule:
                # TODO: if many bascules allow user to select in view
                values['bascule'] = bascule.id
                values['gross_weight'] = bascule.read_measure()
        return values

    def transition_pre_data(self):
        pool = Pool()
        UL = pool.get('stock.unit_load')
        User = pool.get('res.user')

        res = super().transition_pre_data()
        if not self.model or self.model.__name__ == 'ir.ui.menu':
            user = User(Transaction().user)
            bascule = user.get_bascule(UL._get_bascule_pattern())
            if bascule:
                # do not go to data stateview and modify weight
                ul = self.start.unit_load
                ul.gross_weight = bascule.read_measure()
                ul.on_change_gross_weight()
                ul.save()
                self.start.last_unit_load = self.start.unit_load
                return 'pre_start'
        return res

    def default_start(self, fields):
        res = {}
        if getattr(self.start, 'last_unit_load', None):
            ul = self.start.last_unit_load
            res.update({
                'last_unit_load': ul.id,
                'last_gross_weight': ul.gross_weight,
                'last_net_weight': ul.net_weight,
                'last_tare': ul.tare,
                'weight_unit_digits': ul.weight_unit_digits
            })
        return res
