import argparse
import xmlrpc.client
import serial
import sys

HOST = '<>'
PORT = '8000'
DB = '<>'
USER = '<>'
PASSWORD = '<>'

COM_PORT = 'COM1'
BAUD = 19200


class Tryton(object):

    def __init__(self):
        # Get user_id and session
        self.server = xmlrpc.client.ServerProxy('http://%s:%s@%s:%s/%s/' % (
            USER, PASSWORD, HOST, PORT, DB), allow_none=True)

        # Get the user context
        self.pref = self.server.model.res.user.get_preferences(True, {})

    def execute(self, method, *args):
        args += (self.pref,)
        return getattr(self.server, method)(*args)


def parse_commandline():
    parser = argparse.ArgumentParser(prog='create_ul_automaton')
    parser.add_argument("-d", "--debug", dest="debug", action="store_true",
        help="specify if run in debug mode")
    parser.add_argument("-s", "--send-codes", dest="codes", nargs='+',
        default=[], help="specify if run in debug mode")

    options = parser.parse_args()

    if not options.codes and options.debug:
        parser.error('Missing send-codes option')
    return options


if __name__ == '__main__':
    options = parse_commandline()

    a = Tryton()

    if not options.debug:
        ser = serial.Serial(COM_PORT, BAUD, timeout=0,
            parity=serial.PARITY_NONE, rtscts=1)

    ii = 0
    while (not options.debug) or (options.debug and ii < len(options.codes)):
        try:
            if not options.debug:
                value = ser.readline()
            else:
                value = options.codes[ii]

            value = value.strip()
            if value:
                print('[+] Sending "%s"' % value)
                a.execute('model.stock.unit_load.weigh_from_bascule', value)

                print('[+] UL weighted.')
        except Exception:
            e = sys.exc_info()[0]
            print(sys.exc_info()[1])
        finally:
            if options.debug:
                ii += 1
