# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import unit_load


def register():
    Pool.register(
        unit_load.Unitload,
        unit_load.UnitLoadWeightData,
        unit_load.UnitLoadWeightStart,
        module='stock_unit_load_weight_bascule', type_='model')
    Pool.register(
        unit_load.WeighUL,
        module='stock_unit_load_weight_bascule', type_='wizard')
